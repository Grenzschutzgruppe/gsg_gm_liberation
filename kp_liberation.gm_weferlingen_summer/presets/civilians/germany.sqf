/*
Needed Mods:
- None

Optional Mods:
- None
*/

// Civilian classnames.
civilians = [
	"gm_gc_civ_man_01_80_blk",
	"gm_gc_civ_man_01_80_blu",
	"gm_gc_civ_man_02_80_brn",
	"gm_gc_civ_man_02_80_gry",
	"gm_ge_dbp_man_01_80_blu",
	"gm_ge_ff_man_80_orn",
	"C_man_w_worker_F"
];

// Civilian vehicle classnames.
civilian_vehicles = [
	"gm_gc_civ_p601",
	"gm_gc_dp_p601",
	"gm_gc_ff_p601",
	"gm_ge_civ_typ1200",
	"gm_ge_ff_typ1200",
	"gm_ge_dbp_typ1200",
	"gm_xx_civ_bicycle_01",
	"gm_ge_bgs_k125",
	"C_Truck_02_fuel_F",
	"C_Truck_02_transport_F",
	"C_Truck_02_covered_F",	
	"C_Truck_02_box_F"
];
